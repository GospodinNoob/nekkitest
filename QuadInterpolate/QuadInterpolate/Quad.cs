﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


public class InterpolateMethod
{

    private class Point
    {

        public double x;
        public double y;

        public Point(double a, double b)
        {
            this.x = a;
            this.y = b;
        }

    };

    private class Segment
    {

        private double start;
        private double mid;
        private double finish;

        public bool Contains1(double c)
        {
            if ((c >= start) && (c <= mid))
            {
                return true;
            }
            return false;
        }

        public bool Contains2(double c)
        {
            if ((c >= mid) && (c <= finish))
            {
                return true;
            }
            return false;
        }

        public Polynom function;

        public Segment(Point a, Point b, Point c)
        {
            this.start = a.x;
            this.finish = c.x;
            this.function = new Polynom(a, b, c);
        }

        public double Calculate(double va)
        {
            return function.GetValue(va);
        }

    };

    private class Polynom
    {
        private double a;
        private double b;
        private double c;

        public Polynom(Point fir, Point sec, Point last)
        {
            this.a = (last.y - fir.y) / ((last.x - fir.x) * (last.x - sec.x)) - (sec.y - fir.y) / ((sec.x - fir.x) * (last.x - sec.x));
            this.b = (sec.y - fir.y) / (sec.x - fir.x) - this.a * (sec.x + fir.x);
            this.c = fir.y - this.b * fir.x - this.a * fir.x * fir.x;
        }

        public double GetValue(double val)
        {
            return this.a * val * val + this.b * val + this.c;
        }
    };

    private string firstName = "";
    private string secondName = "";
    private string outputName = "";

    public void Interpolate(string n1, string n2, string output)
    {

        firstName = n1 + ".txt";
        secondName = n2 + ".txt";
        outputName = output;

        FileStream file1 = new FileStream(firstName, FileMode.Open);
        FileStream file2 = new FileStream(secondName, FileMode.Open);

        StreamReader reader1 = new StreamReader(file1);
        StreamReader reader2 = new StreamReader(file2);
        FileStream fileOut = new FileStream(outputName, FileMode.Create);
        StreamWriter writer = new StreamWriter(fileOut);
        int len = int.Parse(reader1.ReadLine());
        int lenReq = int.Parse(reader2.ReadLine());

        double[] requipments = new double[lenReq];
        Point[] points = new Point[len];

        for (int i = 0; i < len; i++)
        {
            string st = reader1.ReadLine();
            double[] read = st
                .Split(new char[] { ' ', '\n' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(n => double.Parse(n))
                .ToArray();

            points[i] = new Point(read[0], read[1]);

        }

        for (int i = 0; i < lenReq; i++)
        {
            string st = (reader2.ReadLine());
            requipments[i] = double.Parse(st);
        }

        Segment[] segments = new Segment[len];

        for (int i = 0; i < len - 2; i++)
        {
            segments[i] = new Segment(points[i], points[i + 1], points[i + 2]);
        }


        writer.Write(lenReq);
        writer.WriteLine();

        for (int i = 0; i < lenReq; i++)
        {
            bool tf = false;
            for (int j = 0; j < len - 2; j++)
            {
                if (segments[j].Contains1(requipments[i]))
                {
                    tf = true;
                    double result = segments[j].Calculate(requipments[i]);
                    writer.Write(requipments[i].ToString() + " " + result.ToString());
                    writer.WriteLine();
                    break;
                }
            }
            if (!tf)
            {
                for (int j = 0; j < len - 2; j++)
                {
                    if (segments[j].Contains2(requipments[i]))
                    {
                        tf = true;
                        double result = segments[j].Calculate(requipments[i]);
                        writer.Write(requipments[i].ToString() + " " + result.ToString());
                        writer.WriteLine();
                        break;
                    }
                }
            }
        }

        writer.Close();
        reader1.Close();
        reader2.Close();

    }

}
