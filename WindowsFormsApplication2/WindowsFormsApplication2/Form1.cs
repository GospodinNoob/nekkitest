﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {

        private string linearName = "";
        private string squareName = "";
        private string cubeName = "";

        private string firstFileName = "";
        private string secondFileName = "";
        private string ownLibraryName = "";

        private int libraryFlag = 0;

        public Form1()
        {
            InitializeComponent();
            libraryFlag = 0;
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            firstFileName = textBox1.Text;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            secondFileName = textBox2.Text;
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            ownLibraryName = textBox3.Text;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            libraryFlag = 0;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            libraryFlag = 0;
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            libraryFlag = 0;
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            libraryFlag = 0;
        }

        Assembly dll;

        private void button1_Click(object sender, EventArgs e)
        {
            if (libraryFlag == 0)
            {
                dll = Assembly.LoadFrom(linearName);
            }
        }
    }
}
