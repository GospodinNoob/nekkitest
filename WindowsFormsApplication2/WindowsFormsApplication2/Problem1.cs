﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using System.IO;

namespace WindowsFormsApplication2
{

    public partial class Form1 : Form
    {

        private string linearName = "LinearInterpolate.dll";
        private string squareName = "QuadInterpolate.dll";
        private string cubeName = "";

        private string firstFileName = "";
        private string secondFileName = "";
        private string ownLibraryName = "";

        private int libraryFlag = 0;

        public Form1()
        {
            InitializeComponent();
            libraryFlag = 0;
            textBox1.Text = "Finput";
            textBox2.Text = "Sinput";
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            firstFileName = textBox1.Text;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            secondFileName = textBox2.Text;
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            ownLibraryName = textBox3.Text;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            libraryFlag = 0;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            libraryFlag = 1;
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            libraryFlag = 2;
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            libraryFlag = 3;
        }

        Assembly dll;

        private void button1_Click(object sender, EventArgs e)
        {
            if (libraryFlag == 0)
            {
                dll = Assembly.LoadFrom(linearName);
            }
            if (libraryFlag == 1)
            {
                dll = Assembly.LoadFrom(squareName);
            }
            Object o = dll.CreateInstance("InterpolateMethod");
            Type t = dll.GetType("InterpolateMethod");
            MethodInfo met = t.GetMethod("Interpolate");
            Object[] names = { firstFileName, secondFileName, firstFileName + secondFileName + "Output.txt" };
            //InterpolateMethod me = new InterpolateMethod();
            //me.Interpolate((string)names[0], (string)names[1], (string)names[2]);
            met.Invoke(o, names);
            label3.Text = firstFileName + secondFileName + "Output.txt";
        }
    }
}
